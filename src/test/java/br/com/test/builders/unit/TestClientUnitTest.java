package br.com.test.builders.unit;

import static org.mockito.Mockito.when;

import java.time.LocalDate;

import br.com.builders.entity.Cliente;
import br.com.builders.repository.ClienteRepository;
import br.com.builders.service.ClienteService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@DisplayName("Client test")
@MockitoSettings(strictness = Strictness.LENIENT)
public class TestClientUnitTest {

	@Mock
	private ClienteRepository mockClientRepository;

	@Mock
	private ClienteService mockClientService;

	@Test
	@DisplayName("Should create a client")
	public void shouldCreateAClient() {
		Cliente client = mockClient();
		mockClientRepository.save(client);
	}
	
	@Test
	@DisplayName("Should find a client")
	public void shouldFindAClient() {
		Cliente client = mockClient();
	}
	
	private Cliente mockClient() {
		Cliente client = new Cliente();
		client.setNome("Jose");
		client.setDataNascimento(LocalDate.of(1987, 06, 23));
		client.setCpf("99888075689");
		return client;
	}

}

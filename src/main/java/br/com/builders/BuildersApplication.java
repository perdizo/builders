package br.com.builders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = { "br.com.builders" })
@ComponentScan(basePackages = "br.*")
@EntityScan("br.com.builders.entity")
public class BuildersApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildersApplication.class, args);
	}

}

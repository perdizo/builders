package br.com.builders.service;

import br.com.builders.entity.Cliente;

import java.time.LocalDate;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public interface ClienteService {

	public Page<Cliente> findAll(String nome, String cpf, LocalDate dataNascimento, Integer page, Integer size); 
	
	public Cliente save(Cliente cliente); 
	
	public Optional<Cliente> update(Cliente cliente); 
	
	public void delete(Cliente cliente); 	
	
}

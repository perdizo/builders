package br.com.builders.service.impl;

import br.com.builders.entity.Cliente;
import br.com.builders.repository.ClienteRepository;
import br.com.builders.service.ClienteService;

import java.time.LocalDate;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Override
	public Page<Cliente> findAll(String nome, String cpf, LocalDate dataNascimento, Integer page, Integer size) {
		
		PageRequest pageRequest = PageRequest.of(
                page,
                size,
                Sort.Direction.ASC, 
                "nome");
        return clienteRepository.findAll(nome, cpf, dataNascimento, pageRequest);
	}
	
	@Override
	public Cliente save(Cliente cliente) {

		return Optional.ofNullable(cliente)
				.filter(param -> !clienteRepository.existsById(cliente.getCpf()))
				.map(param -> clienteRepository.save(cliente))
				.orElseGet(() -> new Cliente());
	}

	@Override
	public Optional<Cliente> update(Cliente cliente) {
		
		return clienteRepository.findById(cliente.getCpf()).map(param -> {
			param.setNome(cliente.getNome());
			param.setCpf(cliente.getCpf());
			param.setDataNascimento(cliente.getDataNascimento());
			return clienteRepository.save(param);
		});
	}

	@Override
	public void delete(Cliente cliente) {
		clienteRepository.delete(cliente);
	}

}

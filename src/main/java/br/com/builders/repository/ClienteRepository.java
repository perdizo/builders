package br.com.builders.repository;

import java.time.LocalDate;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.builders.entity.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, String>{

	@Query("FROM Cliente c "
			+ "WHERE cpf = :cpf OR nome like '%:nome%' OR dataNascimento like '%:dataNascimento%'")
	Page<Cliente> findAll(@Param("nome") String nome, @Param("cpf") String cpf, @Param("dataNascimento") LocalDate dataNascimento, Pageable Pageable);
	
}

package br.com.builders.utils;

import java.time.LocalDate;
import java.time.Period;

public class ClientUtils { 
	
	public static Integer getAnos(Integer year, Integer month, Integer day) {
		return Period.between(
				LocalDate.of(year, month, day),
	            LocalDate.now()
	        ).getYears();
	}
	
}
